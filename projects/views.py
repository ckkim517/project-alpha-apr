from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# Create your views here.


# a view that will get all of the instances of the Project model
# put them in the context for the template
@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    # every key in the context dictionary is a variable in the html template
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/list.html", context)


# a view that shows the details of a particular project
@login_required
def show_project(request, id):
    show_project = get_object_or_404(Project, id=id)
    context = {
        "show_project": show_project,
    }
    return render(request, "projects/detail.html", context)


# a create view for the Project model to create a new Project
@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
